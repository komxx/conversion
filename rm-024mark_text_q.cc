#!/bin/sh
. ../util.sh
commit="git rev-parse master^{/New.mark.handling.questions}"

git checkout `$commit`^
git rm libconnection/024mark_text_q.cc
GIT_AUTHOR_NAME="Git conversion" GIT_AUTHOR_EMAIL=nobody@lysator.liu.se GIT_AUTHOR_DATE="1993-08-11 21:29:00" git commit -m"Git-conversion file removal.

Removed 024mark_text_q.cc, which should have been removed in the
previous commit, but since it was removed before CVS had proper file
removal support that was lost in the conversion to Subversion."


echo `$commit` `git rev-parse HEAD` >> .git/info/grafts
git checkout master
filter_rm "$commit" libconnection/024mark_text_q.cc
gc
