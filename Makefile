# Makefile for converting the kom++ repo from Subversion to Git.

GIT = TERM=dumb PATH=/opt/git/bin:/opt/bin:$(shell pwd)/git-svn-abandon:$$PATH git --no-pager
CVS = cvs -d cvs.lysator.liu.se:/cvsroot/kom++
export GIT_COMMITTER_EMAIL = ceder@lysator.liu.se
export PATH := /opt/git/bin:/opt/bin:$(PATH)

all: 99.stamp

clone:
	echo '#!/bin/sh' >> $@.tmp
	echo 'set -e' >> $@.tmp
	echo 'rm -rf $$2-kom++.git' >> $@.tmp
	echo 'cp -a $$1-kom++.git $$2-kom++.git' >> $@.tmp
	echo 'cd $$2-kom++.git || exit 1' >> $@.tmp
	echo 'git rev-parse  --symbolic --glob=original |' >> $@.tmp
	echo 'while read ref; do git update-ref -d $$ref; done' >> $@.tmp
	echo 'git status' >> $@.tmp
	chmod +x $@.tmp
	mv $@.tmp $@

git-svn-abandon.stamp:
	rm -rf git-svn-abandon
	git clone git://github.com/nothingmuch/git-svn-abandon.git
	touch $@

00.stamp: kom++-svn-repo.tar.gz
	rm -rf kom++
	tar xfz kom++-svn-repo.tar.gz
	touch $@

01.stamp: AUTHORS 00.stamp
	rm -rf 01-kom++.git
	git svn clone --prefix=svn/ --stdlayout --authors-file=AUTHORS \
	    file:///`pwd`/kom++/ 01-kom++.git
	touch $@

02.stamp: 01.stamp clone
	./clone 01 02
	cd 02-kom++.git && git svn create-ignore
	cd 02-kom++.git && git commit -m"Convert svn:ignore to .gitignore."
	touch $@

12.stamp: 02.stamp clone fix-subdir
	git --version
	$(GIT) --version
	./clone 02 12
	cd 12-kom++.git && ../fix-subdir
	touch $@

13.stamp: 12.stamp clone fix-isc-branch libisc-0.98.4.tar.gz
	./clone 12 13
	cd 13-kom++.git && ../fix-isc-branch
	touch $@

14.stamp: 13.stamp clone fix-python
	./clone 13 14
	cd 14-kom++.git && ../fix-python
	touch $@

15.stamp: 14.stamp clone fix-tcl
	./clone 14 15
	cd 15-kom++.git && ../fix-tcl
	touch $@

88.stamp: 44.stamp clone
	./clone 44 88
	cd 88-kom++.git && git svn create-ignore
	exit 1

21.stamp: 15.stamp clone rm-files
	./clone 15 21
	cd 21-kom++.git && ../rm-files
	touch $@

22.stamp: 21.stamp clone rm-024mark_text_q.cc
	./clone 21 22
	cd 22-kom++.git && ../rm-024mark_text_q.cc
	touch $@

23.stamp: 22.stamp clone rm-ses-q
	./clone 22 23
	cd 23-kom++.git && ../rm-ses-q
	touch $@

24.stamp: 23.stamp clone rm-time-q
	./clone 23 24
	cd 24-kom++.git && ../rm-time-q
	touch $@

25.stamp: 24.stamp clone rm-initial
	./clone 24 25
	cd 25-kom++.git && ../rm-initial
	touch $@

26.stamp: 25.stamp clone rm-nilkom
	./clone 25 26
	cd 26-kom++.git && ../rm-nilkom
	touch $@

27.stamp: 26.stamp clone rm-parse-vector
	./clone 26 27
	cd 27-kom++.git && ../rm-parse-vector
	touch $@

28.stamp: 27.stamp clone empty-msg
	./clone 27 28
	cd 28-kom++.git && ../empty-msg
	touch $@

29.stamp: 28.stamp clone rm-tkom2
	./clone 28 29
	cd 29-kom++.git && ../rm-tkom2
	touch $@

30.stamp: 29.stamp clone rm-test-tcl
	./clone 29 30
	cd 30-kom++.git && ../rm-test-tcl
	touch $@

31.stamp: 30.stamp clone rm-test-cc
	./clone 30 31
	cd 31-kom++.git && ../rm-test-cc
	touch $@

32.stamp: 31.stamp clone rm-tkom-tcl
	./clone 31 32
	cd 32-kom++.git && ../rm-tkom-tcl
	touch $@

33.stamp: 32.stamp clone rm-makefile
	./clone 32 33
	cd 33-kom++.git && ../rm-makefile
	touch $@

34.stamp: 33.stamp clone rm-makefile-in
	./clone 33 34
	cd 34-kom++.git && ../rm-makefile-in
	touch $@

35.stamp: 34.stamp clone rm-configure
	./clone 34 35
	cd 35-kom++.git && ../rm-configure
	touch $@

36.stamp: 35.stamp clone rm-genclasses
	./clone 35 36
	cd 36-kom++.git && ../rm-genclasses
	touch $@

37.stamp: 36.stamp clone rm-genclasses-mship
	./clone 36 37
	cd 37-kom++.git && ../rm-genclasses-mship
	touch $@

38.stamp: 37.stamp clone rm-automkindex
	./clone 37 38
	cd 38-kom++.git && ../rm-automkindex
	touch $@

39.stamp: 38.stamp clone rm-tk-main
	./clone 38 39
	cd 39-kom++.git && ../rm-tk-main
	touch $@

40.stamp: 39.stamp clone rm-tkom-windows
	./clone 39 40
	cd 40-kom++.git && ../rm-tkom-windows
	touch $@

42.stamp: 40.stamp clone rm-isc-printf-master
	./clone 40 42
	cd 42-kom++.git && ../rm-isc-printf-master
	touch $@

43.stamp: 42.stamp clone rm-comment-order
	./clone 42 43
	cd 43-kom++.git && ../rm-comment-order
	touch $@

44.stamp: 43.stamp clone fix-pepsi
	./clone 43 44
	cd 44-kom++.git && ../fix-pepsi
	touch $@

89.stamp: 44.stamp clone fix-merges
	./clone 44 89
	cd 89-kom++.git && ../fix-merges
	touch $@

90.stamp: 89.stamp clone git-svn-abandon.stamp run-svn-abandon
	./clone 89 90
	cd 90-kom++.git && ../run-svn-abandon
	touch $@

91.stamp: 90.stamp clone git-svn-abandon.stamp
	./clone 90 91
	cd 91-kom++.git && $(GIT) svn-abandon-cleanup
	rm -f 91-kom++.git/.git/info/grafts
	touch $@

92.stamp: 91.stamp clone rm-dummy-roots
	./clone 91 92
	cd 92-kom++.git && ../rm-dummy-roots
	touch $@

93.stamp: 92.stamp clone fix-07post4
	./clone 92 93
	cd 93-kom++.git && ../fix-07post4
	touch $@

94.stamp: 93.stamp clone last-tags
	./clone 93 94
	cd 94-kom++.git && ../last-tags
	touch $@

95.stamp: 94.stamp clone fix-commit-msgs-filter fix-commit-msgs
	./clone 94 95
	cd 95-kom++.git && ../fix-commit-msgs
	touch $@

96.stamp: 95.stamp clone release-tags
	./clone 95 96
	cd 96-kom++.git && ../release-tags
	touch $@

99.stamp: 96.stamp
	./clone 96 99
	touch $@

ISCURL = ftp://ftp.lysator.liu.se/pub/unix/isc/

libisc-0.98.4.tar.gz:
	wget http://people.ifm.liu.se/peter/isc/libisc-0.98.4.tar.gz

isc-0.99.tar.gz:
	wget $(ISCURL)/@$

isc-0.99.tar.gz.sig:
	wget $(ISCURL)/@$

isc-1.00.tar.gz:
	wget $(ISCURL)/@$

isc-1.00.tar.gz.sig:
	wget $(ISCURL)/@$

isc-1.01.tar.gz:
	wget $(ISCURL)/@$

isc-1.01.tar.gz.sig:
	wget $(ISCURL)/@$

isc-0.99.stamp: isc-0.99.tar.gz isc-0.99.tar.gz.sig
	gpg --verify isc-0.99.tar.gz.sig isc-0.99.tar.gz
	touch $@

isc-1.00.stamp: isc-1.00.tar.gz isc-1.00.tar.gz.sig
	gpg --verify isc-1.00.tar.gz.sig isc-1.00.tar.gz
	touch $@

isc-1.01.stamp: isc-1.01.tar.gz isc-1.01.tar.gz.sig
	gpg --verify isc-1.01.tar.gz.sig isc-1.01.tar.gz
	touch $@

isc.stamp: isc-0.99.stamp isc-1.00.stamp isc-1.01.stamp

KOMPPURL = ftp://ftp.lysator.liu.se/pub/lyskom/kom++/

kom++-0.1.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.2.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.3.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.4.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.5.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.6.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.7.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.7.post.4.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.7.post.5.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.7.post.6.tar.gz:
	wget $(KOMPPURL)/$@

kom++-0.1.raw.stamp: kom++-0.1.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.1.stamp: kom++-0.1.raw.stamp 0.1-dist.patch
	rm -rf kom++-0.1
	cp -a raw/kom++-0.1 kom++-0.1
	rm kom++-0.1/libisc/src/dependencies
	rm kom++-0.1/stmp_libs
	rmdir kom++-0.1/error-hiders
	(cd kom++-0.1 && patch -p1) < 0.1-dist.patch
	touch $@

kom++-0.2.raw.stamp: kom++-0.2.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.2.stamp: kom++-0.2.raw.stamp
	rm -rf kom++-0.2
	cp -a raw/kom++-0.2 kom++-0.2
	rm kom++-0.2/libisc/src/dependencies
	rm kom++-0.2/stmp_libs
	rm kom++-0.2/tcl-clients/TAGS
	rm kom++-0.2/tcl-clients/tclIndex
	rmdir kom++-0.2/error-hiders
	touch $@

kom++-0.3.raw.stamp: kom++-0.3.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.3.stamp: kom++-0.3.raw.stamp
	rm -rf kom++-0.3
	cp -a raw/kom++-0.3 kom++-0.3
	rm kom++-0.3/Makefile.in
	rm kom++-0.3/configure
	rm kom++-0.3/*/Makefile.in
	rm kom++-0.3/*/*/Makefile.in
	touch $@

kom++-0.4.raw.stamp: kom++-0.4.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.4.stamp: kom++-0.4.raw.stamp
	rm -rf kom++-0.4
	cp -a raw/kom++-0.4 kom++-0.4
	rm kom++-0.4/Makefile.in
	rm kom++-0.4/configure
	rm kom++-0.4/genclasses/Makefile.in
	rm kom++-0.4/libconnection/Makefile.in
	rm kom++-0.4/libisc/Makefile.in
	rm kom++-0.4/libisc/src/Makefile.in
	rm kom++-0.4/libkom++/Makefile.in
	rm kom++-0.4/libtcl/configure
	rm kom++-0.4/libtcl/unix/Makefile.bak
	rm kom++-0.4/py-clients/Makefile.in
	rm kom++-0.4/tcl-clients/Makefile.in
	touch $@

kom++-0.5.raw.stamp: kom++-0.5.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.5.stamp: kom++-0.5.raw.stamp 0.5-dist.patch
	rm -rf kom++-0.5
	cp -a raw/kom++-0.5 kom++-0.5
	(cd kom++-0.5 && patch -p1) < 0.5-dist.patch
	rm kom++-0.5/Makefile.in
	rm kom++-0.5/configure
	rm kom++-0.5/rebuild
	rm kom++-0.5/genclasses/Makefile.in
	rm kom++-0.5/libconnection/Makefile.in
	rm kom++-0.5/libisc/Makefile.in
	rm kom++-0.5/libisc/src/Makefile.in
	rm kom++-0.5/libkom++/Makefile.in
	rm kom++-0.5/libtcl/configure
	rm kom++-0.5/py-clients/Makefile.in
	rm kom++-0.5/py-clients/komerr.py
	rm kom++-0.5/tcl-clients/nilkominit.c
	rm kom++-0.5/tcl-clients/Makefile.in
	rm kom++-0.5/www/Makefile.in
	rm kom++-0.5/www/lm
	rm kom++-0.5/www/local5.log
	rm kom++-0.5/www/postinstall
	rm kom++-0.5/www/html/Makefile.in
	rm kom++-0.5/www/testsuite/Makefile.in
	rm kom++-0.5/www/testsuite/dbg.log
	rm kom++-0.5/www/testsuite/komconfig.py
	rm kom++-0.5/www/testsuite/lm.log
	rm kom++-0.5/www/testsuite/lm.sum
	rm kom++-0.5/www/testsuite/lm_start
	rm -f kom++-0.5/www/testsuite/site.exp
	rm kom++-0.5/www/komconfig.py
	rm kom++-0.5/libkom++/memcheck-enabler.h
	touch $@

kom++-0.6.raw.stamp: kom++-0.6.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.6.stamp: kom++-0.6.raw.stamp mangle-vendor-branch-rev
	rm -rf kom++-0.6
	cp -a raw/kom++-0.6 kom++-0.6
	cd kom++-0.6 && ../mangle-vendor-branch-rev
	rm kom++-0.6/Makefile.in
	rm kom++-0.6/configure
	rm kom++-0.6/rebuild
	rm kom++-0.6/genclasses/Makefile.in
	rm kom++-0.6/libconnection/Makefile.in
	rm kom++-0.6/libisc/Makefile.in
	rm kom++-0.6/libisc/src/Makefile.in
	rm kom++-0.6/libkom++/Makefile.in
	rm kom++-0.6/libtcl/configure
	rm kom++-0.6/py-clients/Makefile.in
	rm kom++-0.6/python/Modules/Makefile.pre
	rm kom++-0.6/python/Modules/Setup
	rm kom++-0.6/python/Modules/config.c
	rm kom++-0.6/python/Parser/pgen
	rm kom++-0.6/python/config.h
	rm kom++-0.6/tcl-clients/nilkominit.c
	rm kom++-0.6/tcl-clients/Makefile.in
	rm kom++-0.6/tcl-clients/.gdbinit
	rm kom++-0.6/www/Makefile.in
	rm kom++-0.6/www/lm
	rm kom++-0.6/www/postinstall
	rm kom++-0.6/www/html/Makefile.in
	rm kom++-0.6/www/testsuite/Makefile.in
	rm kom++-0.6/www/testsuite/komconfig.py
	rm kom++-0.6/www/testsuite/config/komconfig.py
	rm kom++-0.6/www/testsuite/.gdbinit
	rm -f kom++-0.6/www/testsuite/lm_start
	rm -f kom++-0.6/www/testsuite/py_start
	rm -f kom++-0.6/www/testsuite/sc_start
	rm -f kom++-0.6/www/testsuite/logger
	rm -f kom++-0.6/www/testsuite/site.exp
	rm kom++-0.6/www/komconfig.py
	rm kom++-0.6/libkom++/memcheck-enabler.h
	touch $@

kom++-0.7.raw.stamp: kom++-0.7.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.7.stamp: kom++-0.7.raw.stamp mangle-vendor-branch-rev
	rm -rf kom++-0.7
	cp -a raw/kom++-0.7 kom++-0.7
	cd kom++-0.7 && ../mangle-vendor-branch-rev
	rm kom++-0.7/Makefile.in
	rm kom++-0.7/configure
	rm kom++-0.7/rebuild
	rm kom++-0.7/genclasses/Makefile.in
	rm kom++-0.7/libconnection/Makefile.in
	rm kom++-0.7/libisc/Makefile.in
	rm kom++-0.7/libisc/src/Makefile.in
	rm kom++-0.7/libkom++/Makefile.in
	rm kom++-0.7/libtcl/configure
	rm kom++-0.7/py-clients/Makefile.in
	rm kom++-0.7/tcl-clients/Makefile.in
	rm kom++-0.7/www/Makefile.in
	rm kom++-0.7/www/html/Makefile.in
	rm kom++-0.7/www/testsuite/Makefile.in
	touch $@

kom++-0.7.post.4.raw.stamp: kom++-0.7.post.4.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.7.post.4.stamp: kom++-0.7.post.4.raw.stamp mangle-vendor-branch-rev
	rm -rf kom++-0.7.post.4
	cp -a raw/kom++-0.7.post.4 kom++-0.7.post.4
	cd kom++-0.7.post.4 && ../mangle-vendor-branch-rev
	rm kom++-0.7.post.4/tcl-clients/nilkominit.c
	rm kom++-0.7.post.4/Makefile.in
	rm kom++-0.7.post.4/aclocal.m4
	rm kom++-0.7.post.4/stamp-h.in
	rm kom++-0.7.post.4/missing
	rm kom++-0.7.post.4/config.h.in
	rm kom++-0.7.post.4/configure
	rm kom++-0.7.post.4/install-sh
	rm kom++-0.7.post.4/libcompat/Makefile.in
	rm kom++-0.7.post.4/libkom++/Makefile.in
	rm kom++-0.7.post.4/libconnection/Makefile.in
	rm kom++-0.7.post.4/libisc/Makefile.in
	rm kom++-0.7.post.4/libisc/aclocal.m4
	rm kom++-0.7.post.4/libisc/configure
	rm kom++-0.7.post.4/libisc/src/Makefile.in
	rm kom++-0.7.post.4/libisc/demo/Makefile.in
	rm kom++-0.7.post.4/libisc/doc/Makefile.in
	rm kom++-0.7.post.4/libisc/man/Makefile.in
	rm kom++-0.7.post.4/other-clients/Makefile.in
	rm kom++-0.7.post.4/tcl-clients/Makefile.in
	touch $@

kom++-0.7.post.5.raw.stamp: kom++-0.7.post.5.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.7.post.5.stamp: kom++-0.7.post.5.raw.stamp mangle-vendor-branch-rev
	rm -rf kom++-0.7.post.5
	cp -a raw/kom++-0.7.post.5 kom++-0.7.post.5
	cd kom++-0.7.post.5 && ../mangle-vendor-branch-rev
	rm kom++-0.7.post.5/INSTALL
	rm kom++-0.7.post.5/Makefile.in
	rm kom++-0.7.post.5/aclocal.m4
	rm kom++-0.7.post.5/config.h.in
	rm kom++-0.7.post.5/configure
	rm kom++-0.7.post.5/install-sh
	rm kom++-0.7.post.5/libcompat/Makefile.in
	rm kom++-0.7.post.5/libconnection/Makefile.in
	rm kom++-0.7.post.5/libisc/Makefile.in
	rm kom++-0.7.post.5/libisc/aclocal.m4
	rm kom++-0.7.post.5/libisc/configure
	rm kom++-0.7.post.5/libisc/demo/Makefile.in
	rm kom++-0.7.post.5/libisc/doc/Makefile.in
	rm kom++-0.7.post.5/libisc/man/Makefile.in
	rm kom++-0.7.post.5/libisc/src/Makefile.in
	rm kom++-0.7.post.5/libkom++/Makefile.in
	rm kom++-0.7.post.5/missing
	rm kom++-0.7.post.5/other-clients/Makefile.in
	rm kom++-0.7.post.5/stamp-h.in
	rm kom++-0.7.post.5/tcl-clients/Makefile.in
	rm kom++-0.7.post.5/tcl-clients/nilkominit.c
	rm -f kom++-0.7.post.5/tcl-clients/acinclude.m4
	rm kom++-0.7.post.5/tcl-clients/aclocal.m4
	rm kom++-0.7.post.5/tcl-clients/config.h.in
	rm kom++-0.7.post.5/tcl-clients/configure
	rm kom++-0.7.post.5/tcl-clients/stamp-h.in
	rm kom++-0.7.post.5/py-clients/Makefile.in
	rm kom++-0.7.post.5/www/Makefile.in
	rm kom++-0.7.post.5/www/html/Makefile.in
	rm kom++-0.7.post.5/www/genconfig
	rm kom++-0.7.post.5/doc/komxx.info
	rm kom++-0.7.post.5/doc/Makefile.in
	rm kom++-0.7.post.5/doc/texinfo.tex
	touch $@

kom++-0.7.post.6.raw.stamp: kom++-0.7.post.6.tar.gz
	mkdir -p raw
	tar xfz $< -C raw
	touch $@

kom++-0.7.post.6.stamp: kom++-0.7.post.6.raw.stamp mangle-vendor-branch-rev
	rm -rf kom++-0.7.post.6
	cp -a raw/kom++-0.7.post.6 kom++-0.7.post.6
	cd kom++-0.7.post.6 && ../mangle-vendor-branch-rev
	rm kom++-0.7.post.6/INSTALL
	rm kom++-0.7.post.6/Makefile.in
	rm kom++-0.7.post.6/aclocal.m4
	rm kom++-0.7.post.6/config.h.in
	rm kom++-0.7.post.6/configure
	rm kom++-0.7.post.6/install-sh
	rm kom++-0.7.post.6/depcomp
	rm kom++-0.7.post.6/libcompat/Makefile.in
	rm kom++-0.7.post.6/libconnection/Makefile.in
	rm kom++-0.7.post.6/libisc/Makefile.in
	rm kom++-0.7.post.6/libisc/aclocal.m4
	rm kom++-0.7.post.6/libisc/configure
	rm kom++-0.7.post.6/libisc/demo/Makefile.in
	rm kom++-0.7.post.6/libisc/doc/Makefile.in
	rm kom++-0.7.post.6/libisc/man/Makefile.in
	rm kom++-0.7.post.6/libisc/src/Makefile.in
	rm kom++-0.7.post.6/libkom++/Makefile.in
	rm kom++-0.7.post.6/missing
	rm kom++-0.7.post.6/other-clients/Makefile.in
	rm kom++-0.7.post.6/tcl-clients/Makefile.in
	rm kom++-0.7.post.6/tcl-clients/nilkominit.c
	rm -f kom++-0.7.post.6/tcl-clients/acinclude.m4
	rm kom++-0.7.post.6/tcl-clients/aclocal.m4
	rm kom++-0.7.post.6/tcl-clients/config.h.in
	rm kom++-0.7.post.6/tcl-clients/configure
	rm kom++-0.7.post.6/py-clients/Makefile.in
	rm kom++-0.7.post.6/www/Makefile.in
	rm kom++-0.7.post.6/www/html/Makefile.in
	rm kom++-0.7.post.6/www/genconfig
	rm kom++-0.7.post.6/doc/komxx.info
	rm kom++-0.7.post.6/doc/Makefile.in
	rm kom++-0.7.post.6/doc/texinfo.tex
	touch $@

unpack-kom++-dists: kom++-0.1.stamp kom++-0.2.stamp \
	kom++-0.3.stamp kom++-0.4.stamp kom++-0.5.stamp \
	kom++-0.6.stamp kom++-0.7.stamp \
	kom++-0.7.post.4.stamp \
	kom++-0.7.post.5.stamp \
	kom++-0.7.post.6.stamp

cvs-0.1-raw.stamp:
	rm -rf cvs/raw/0.1
	mkdir -p cvs/raw/0.1
	cd cvs/raw/0.1 && $(CVS) co -r release-0-1 kom++
	touch $@

cvs-0.1.stamp: cvs-0.1-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids 0.1
	rm cvs/0.1/kom++/libconnection/change_conference_q.h
	rm cvs/0.1/kom++/libconnection/002change_conference_q.cc
	touch $@

cvs-0.3-raw.stamp:
	rm -rf cvs/raw/0.3
	mkdir -p cvs/raw/0.3
	cd cvs/raw/0.3 && $(CVS) co -r tclkom-0-3 kom++
	touch $@

cvs-0.3.stamp: cvs-0.3-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids 0.3
	rm cvs/0.3/kom++/libconnection/change_conference_q.h
	rm cvs/0.3/kom++/libconnection/002change_conference_q.cc
	touch $@

cvs-0.5-raw.stamp:
	rm -rf cvs/raw/0.5
	mkdir -p cvs/raw/0.5
	cd cvs/raw/0.5 && $(CVS) co -r tclkom-0-5 kom++
	touch $@

cvs-0.5.stamp: cvs-0.5-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids 0.5
	rm cvs/0.5/kom++/libconnection/change_conference_q.h
	rm cvs/0.5/kom++/libconnection/002change_conference_q.cc
	touch $@

cvs-0.7-raw.stamp:
	rm -rf cvs/raw/0.7
	mkdir -p cvs/raw/0.7
	cd cvs/raw/0.7 && $(CVS) co -r tclkom-0-7 kom++
	touch $@

cvs-0.7.stamp: cvs-0.7-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids 0.7
	touch $@

cvs-0.7.post.2-raw.stamp:
	rm -rf cvs/raw/0.7.post.2
	mkdir -p cvs/raw/0.7.post.2
	cd cvs/raw/0.7.post.2 && $(CVS) co -r tclkom-0-7-post-2 kom++
	touch $@

cvs-0.7.post.2.stamp: cvs-0.7.post.2-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids 0.7.post.2
	touch $@

cvs-0.7.post.4-raw.stamp:
	rm -rf cvs/raw/0.7.post.4
	mkdir -p cvs/raw/0.7.post.4
	cd cvs/raw/0.7.post.4 && $(CVS) co -r tclkom-0-7-post-4 kom++
	touch $@

cvs-0.7.post.4.stamp: cvs-0.7.post.4-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids 0.7.post.4
	touch $@

cvs-0.7.post.5-raw.stamp:
	rm -rf cvs/raw/0.7.post.5
	mkdir -p cvs/raw/0.7.post.5
	cd cvs/raw/0.7.post.5 && $(CVS) co -r tclkom-0-7-post-5 kom++
	touch $@

cvs-0.7.post.5.stamp: cvs-0.7.post.5-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids 0.7.post.5
	touch $@

cvs-0.7.post.6-raw.stamp:
	rm -rf cvs/raw/0.7.post.6
	mkdir -p cvs/raw/0.7.post.6
	cd cvs/raw/0.7.post.6 && $(CVS) co -r tclkom-0-7-post-6 kom++
	touch $@

cvs-0.7.post.6.stamp: cvs-0.7.post.6-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids 0.7.post.6
	touch $@

cvs-isc-0.98.3-raw.stamp:
	rm -rf cvs/raw/isc-0.98.3
	mkdir -p cvs/raw/isc-0.98.3
	cd cvs/raw/isc-0.98.3 && $(CVS) co -r isc-0-98-3 kom++
	touch $@

cvs-isc-0.98.3.stamp: cvs-isc-0.98.3-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids isc-0.98.3
	touch $@

cvs-isc-1.00-raw.stamp:
	rm -rf cvs/raw/isc-1.00
	mkdir -p cvs/raw/isc-1.00
	cd cvs/raw/isc-1.00 && $(CVS) co -r isc-1-00 kom++
	touch $@

cvs-isc-1.00.stamp: cvs-isc-1.00-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids isc-1.00
	touch $@

cvs-isc-1.01-raw.stamp:
	rm -rf cvs/raw/isc-1.01
	mkdir -p cvs/raw/isc-1.01
	cd cvs/raw/isc-1.01 && $(CVS) co -r isc-1-01 kom++
	touch $@

cvs-isc-1.01.stamp: cvs-isc-1.01-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids isc-1.01
	touch $@

cvs-python-1.4-raw.stamp:
	rm -rf cvs/raw/python-1.4
	mkdir -p cvs/raw/python-1.4
	cd cvs/raw/python-1.4 && $(CVS) co -r guido_1_4 kom++
	touch $@

cvs-python-1.4.stamp: cvs-python-1.4-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids python-1.4
	touch $@

cvs-python-1.4b1-raw.stamp:
	rm -rf cvs/raw/python-1.4b1
	mkdir -p cvs/raw/python-1.4b1
	cd cvs/raw/python-1.4b1 && $(CVS) co -r guido_1_4b1 kom++
	touch $@

cvs-python-1.4b1.stamp: cvs-python-1.4b1-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids python-1.4b1
	touch $@

cvs-python-1.4b3-raw.stamp:
	rm -rf cvs/raw/python-1.4b3
	mkdir -p cvs/raw/python-1.4b3
	cd cvs/raw/python-1.4b3 && $(CVS) co -r guido_1_4b3 kom++
	touch $@

cvs-python-1.4b3.stamp: cvs-python-1.4b3-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids python-1.4b3
	touch $@

cvs-tcl7.5a2-raw.stamp:
	rm -rf cvs/raw/tcl7.5a2
	mkdir -p cvs/raw/tcl7.5a2
	cd cvs/raw/tcl7.5a2 && $(CVS) co -r tcl7_5a2 kom++
	touch $@

cvs-tcl7.5a2.stamp: cvs-tcl7.5a2-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids tcl7.5a2
	touch $@

cvs-ERRHIDE-raw.stamp:
	rm -rf cvs/raw/ERRHIDE
	mkdir -p cvs/raw/ERRHIDE
	cd cvs/raw/ERRHIDE && $(CVS) co -r ERRHIDE kom++
	touch $@

cvs-ERRHIDE.stamp: cvs-ERRHIDE-raw.stamp svn-ERRHIDE.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids ERRHIDE
	cp svn/ERRHIDE/kom++/libkom++/error-hider.h cvs/ERRHIDE/kom++/libkom++/
	cp svn/ERRHIDE/kom++/libkom++/error-hider.cc cvs/ERRHIDE/kom++/libkom++/
	rm cvs/ERRHIDE/kom++/libconnection/change_conference_q.h
	touch $@

cvs-tcl-raw.stamp:
	rm -rf cvs/raw/tcl
	mkdir -p cvs/raw/tcl
	cd cvs/raw/tcl && $(CVS) co -r ousterhout kom++
	touch $@

cvs-tcl.stamp: cvs-tcl-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids tcl
	touch $@

cvs-python-raw.stamp:
	rm -rf cvs/raw/python
	mkdir -p cvs/raw/python
	cd cvs/raw/python && $(CVS) co -r guido kom++
	touch $@

cvs-python.stamp: cvs-python-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids python
	touch $@

cvs-isc-raw.stamp:
	rm -rf cvs/raw/isc
	mkdir -p cvs/raw/isc
	cd cvs/raw/isc && $(CVS) co -r isc kom++
	touch $@

cvs-isc.stamp: cvs-isc-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids isc
	rm -rf cvs/isc/kom++/libisc/build
	rm cvs/isc/kom++/libisc/demo/Makefile
	rm cvs/isc/kom++/libisc/man/Makefile
	rm cvs/isc/kom++/libisc/src/Makefile
	rm cvs/isc/kom++/libisc/src/printf.c
	touch $@

cvs-snapshot-960712-raw.stamp:
	rm -rf cvs/raw/snapshot-960712
	mkdir -p cvs/raw/snapshot-960712
	cd cvs/raw/snapshot-960712 && $(CVS) co -r wwwkom-960712 kom++
	touch $@

cvs-snapshot-960712.stamp: cvs-snapshot-960712-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids snapshot-960712
	rm cvs/snapshot-960712/kom++/libconnection/change_conference_q.h
	rm cvs/snapshot-960712/kom++/libconnection/002change_conference_q.cc
	touch $@

cvs-last-cvs-raw.stamp:
	rm -rf cvs/raw/last-cvs
	mkdir -p cvs/raw/last-cvs
	cd cvs/raw/last-cvs && $(CVS) co -r moved-to-subversion kom++
	touch $@

cvs-last-cvs.stamp: cvs-last-cvs-raw.stamp ancient-ids mangle-vendor-branch-rev
	./ancient-ids last-cvs
	touch $@

cvs-facit: cvs-0.1.stamp cvs-0.3.stamp cvs-0.5.stamp cvs-0.7.stamp \
	cvs-0.7.post.2.stamp cvs-0.7.post.4.stamp cvs-0.7.post.5.stamp \
	cvs-0.7.post.6.stamp cvs-ERRHIDE.stamp cvs-last.stamp

git-0.1.stamp: 99.stamp clone
	./clone 99 0.1
	cd 0.1-kom++.git && git checkout 0.1
	touch $@

git-0.2.stamp: 99.stamp clone
	./clone 99 0.2
	cd 0.2-kom++.git && git checkout 0.2
	touch $@

git-0.3.stamp: 99.stamp clone
	./clone 99 0.3
	cd 0.3-kom++.git && git checkout 0.3
	touch $@

git-0.4.stamp: 99.stamp clone
	./clone 99 0.4
	cd 0.4-kom++.git && git checkout 0.4
	touch $@

git-0.5.stamp: 99.stamp clone
	./clone 99 0.5
	cd 0.5-kom++.git && git checkout 0.5
	touch $@

git-0.6.stamp: 99.stamp clone
	./clone 99 0.6
	cd 0.6-kom++.git && git checkout 0.6
	touch $@

git-0.7.stamp: 99.stamp clone
	./clone 99 0.7
	cd 0.7-kom++.git && git checkout 0.7
	touch $@

git-0.7.post.2.stamp: 99.stamp clone
	./clone 99 0.7.post.2
	cd 0.7.post.2-kom++.git && git checkout 0.7.post.2
	touch $@

git-0.7.post.4.stamp: 99.stamp clone
	./clone 99 0.7.post.4
	cd 0.7.post.4-kom++.git && git checkout 0.7.post.4
	touch $@

git-0.7.post.5.stamp: 99.stamp clone
	./clone 99 0.7.post.5
	cd 0.7.post.5-kom++.git && git checkout 0.7.post.5
	touch $@

git-0.7.post.6.stamp: 99.stamp clone
	./clone 99 0.7.post.6
	cd 0.7.post.6-kom++.git && git checkout 0.7.post.6
	touch $@

git-snapshot-960712.stamp: 99.stamp clone
	./clone 99 snapshot-960712
	cd snapshot-960712-kom++.git && git checkout snapshot-960712
	touch $@

git-ERRHIDE.stamp: 99.stamp clone
	./clone 99 ERRHIDE
	cd ERRHIDE-kom++.git && git checkout ERRHIDE
	touch $@

git-isc.stamp: 99.stamp clone
	./clone 99 isc
	cd isc-kom++.git && git checkout isc
	touch $@

git-python.stamp: 99.stamp clone
	./clone 99 python
	cd python-kom++.git && git checkout python
	touch $@

git-tcl.stamp: 99.stamp clone
	./clone 99 tcl
	cd tcl-kom++.git && git checkout tcl
	touch $@

git-isc-0.98.3.stamp: 99.stamp clone
	./clone 99 isc-0.98.3
	cd isc-0.98.3-kom++.git && git checkout isc-0.98.3
	touch $@

git-isc-1.00.stamp: 99.stamp clone
	./clone 99 isc-1.00
	cd isc-1.00-kom++.git && git checkout isc-1.00
	touch $@

git-isc-1.01.stamp: 99.stamp clone
	./clone 99 isc-1.01
	cd isc-1.01-kom++.git && git checkout isc-1.01
	touch $@

git-python-1.4.stamp: 99.stamp clone
	./clone 99 python-1.4
	cd python-1.4-kom++.git && git checkout python-1.4
	touch $@

git-python-1.4b1.stamp: 99.stamp clone
	./clone 99 python-1.4b1
	cd python-1.4b1-kom++.git && git checkout python-1.4b1
	touch $@

git-python-1.4b3.stamp: 99.stamp clone
	./clone 99 python-1.4b3
	cd python-1.4b3-kom++.git && git checkout python-1.4b3
	touch $@

git-tcl7.5a2.stamp: 99.stamp clone
	./clone 99 tcl7.5a2
	cd tcl7.5a2-kom++.git && git checkout tcl7.5a2
	touch $@

git-last-cvs.stamp: 99.stamp clone
	./clone 99 last-cvs
	cd last-cvs-kom++.git && git checkout last-cvs
	touch $@

git-last-svn.stamp: 99.stamp clone
	./clone 99 last-svn
	cd last-svn-kom++.git && git checkout last-svn
	touch $@

svn-0.1-raw.stamp: 00.stamp
	mkdir -p svn/raw
	rm -rf svn/raw/0.1
	cd svn/raw && svn co "file:///$(shell pwd)/kom++/tags/0.1"
	touch $@

svn-0.1.stamp: svn-0.1-raw.stamp
	rm -rf svn/0.1
	cp -a svn/raw/0.1 svn/0.1
	rm svn/0.1/kom++/libconnection/change_conference_q.h
	rm svn/0.1/kom++/libconnection/002change_conference_q.cc
	touch $@

svn-0.3-raw.stamp: 00.stamp
	mkdir -p svn/raw
	rm -rf svn/raw/0.3
	cd svn/raw && svn co "file:///$(shell pwd)/kom++/tags/0.3"
	touch $@

svn-0.3.stamp: svn-0.3-raw.stamp
	rm -rf svn/0.3
	cp -a svn/raw/0.3 svn/0.3
	rm svn/0.3/kom++/libconnection/change_conference_q.h
	rm svn/0.3/kom++/libconnection/002change_conference_q.cc
	touch $@

svn-0.5-raw.stamp: 00.stamp
	mkdir -p svn/raw
	rm -rf svn/raw/0.5
	cd svn/raw && svn co "file:///$(shell pwd)/kom++/tags/0.5"
	touch $@

svn-0.5.stamp: svn-0.5-raw.stamp
	rm -rf svn/0.5
	cp -a svn/raw/0.5 svn/0.5
	rm svn/0.5/kom++/libconnection/change_conference_q.h
	rm svn/0.5/kom++/libconnection/002change_conference_q.cc
	touch $@

svn-0.7.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/0.7
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/0.7"
	touch $@

svn-0.7.post.2.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/0.7.post.2
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/0.7.post.2"
	touch $@

svn-0.7.post.4.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/0.7.post.4
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/0.7.post.4"
	touch $@

svn-0.7.post.5.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/0.7.post.5
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/0.7.post.5"
	touch $@

svn-0.7.post.6.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/0.7.post.6
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/0.7.post.6"
	touch $@

svn-snapshot-960712-raw.stamp: 00.stamp
	mkdir -p svn/raw
	rm -rf svn/raw/snapshot-960712
	cd svn/raw && svn co "file:///$(shell pwd)/kom++/tags/snapshot-960712"
	touch $@

svn-snapshot-960712.stamp: svn-snapshot-960712-raw.stamp
	rm -rf svn/snapshot-960712
	cp -a svn/raw/snapshot-960712 svn/snapshot-960712
	rm svn/snapshot-960712/kom++/libconnection/change_conference_q.h
	rm svn/snapshot-960712/kom++/libconnection/002change_conference_q.cc
	touch $@

svn-python-1.4.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/python-1.4
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/python-1.4"
	touch $@

svn-python-1.4b1.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/python-1.4b1
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/python-1.4b1"
	touch $@

svn-python-1.4b3.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/python-1.4b3
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/python-1.4b3"
	touch $@

svn-tcl7.5a2.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/tcl7.5a2
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/tcl7.5a2"
	touch $@

svn-ERRHIDE-raw.stamp: 00.stamp
	mkdir -p svn/raw
	rm -rf svn/raw/ERRHIDE
	cd svn/raw && svn co "file:///$(shell pwd)/kom++/branches/ERRHIDE"
	touch $@

svn-ERRHIDE.stamp: svn-ERRHIDE-raw.stamp
	rm -rf svn/ERRHIDE
	cp -a svn/raw/ERRHIDE svn/ERRHIDE
	rm svn/ERRHIDE/kom++/libconnection/change_conference_q.h
	touch $@

svn-tcl.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/tcl
	cd svn && svn co "file:///$(shell pwd)/kom++/branches/tcl"
	touch $@

svn-python.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/python
	cd svn && svn co "file:///$(shell pwd)/kom++/branches/python"
	touch $@

svn-isc-raw.stamp: 00.stamp
	mkdir -p svn/raw
	rm -rf svn/raw/isc
	cd svn/raw && svn co "file:///$(shell pwd)/kom++/branches/isc"
	touch $@

svn-isc.stamp: svn-isc-raw.stamp
	rm -rf svn/isc
	cp -a svn/raw/isc svn/isc
	rm -rf svn/isc/kom++/libisc/build
	rm svn/isc/kom++/libisc/demo/Makefile
	rm svn/isc/kom++/libisc/man/Makefile
	rm svn/isc/kom++/libisc/src/Makefile
	rm svn/isc/kom++/libisc/src/printf.c
	touch $@

svn-isc-0.98.3.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/isc-0.98.3
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/isc-0.98.3"
	touch $@

svn-isc-1.00.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/isc-1.00
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/isc-1.00"
	touch $@

svn-isc-1.01.stamp: 00.stamp
	mkdir -p svn
	rm -rf svn/isc-1.01
	cd svn && svn co "file:///$(shell pwd)/kom++/tags/isc-1.01"
	touch $@

svn-last-cvs-raw.stamp: 00.stamp
	mkdir -p svn/raw
	rm -rf svn/raw/last-cvs
	cd svn/raw && svn co -r 3605 "file:///$(shell pwd)/kom++/trunk" last-cvs
	touch $@

svn-last-cvs.stamp: svn-last-cvs-raw.stamp
	rm -rf svn/last-cvs
	cp -a svn/raw/last-cvs svn/last-cvs
	rm -rf svn/last-cvs/kom++/error-hiders/.svn
	rmdir svn/last-cvs/kom++/error-hiders
	touch $@

svn-last-svn-raw.stamp: 00.stamp
	mkdir -p svn/raw
	rm -rf svn/raw/last-svn
	cd svn/raw && svn co "file:///$(shell pwd)/kom++/trunk" last-svn
	touch $@

svn-last-svn.stamp: svn-last-svn-raw.stamp
	rm -rf svn/last-svn
	cp -a svn/raw/last-svn svn/last-svn
	rm -rf svn/last-svn/kom++/genclasses/.svn
	rm -rf svn/last-svn/kom++/error-hiders/.svn
	rmdir svn/last-svn/kom++/genclasses
	rmdir svn/last-svn/kom++/error-hiders
	touch $@

check-cvs-0.1.stamp: cvs-0.1.stamp git-0.1.stamp
	diff -x CVS -x .git -r --brief cvs/0.1/kom++ 0.1-kom++.git
	touch $@

check-svn-0.1.stamp: svn-0.1.stamp git-0.1.stamp
	diff -x .svn -x .git -r --brief svn/0.1/kom++ 0.1-kom++.git
	touch $@

check-cvs-0.3.stamp: cvs-0.3.stamp git-0.3.stamp
	diff -x CVS -x .git -r --brief cvs/0.3/kom++ 0.3-kom++.git
	touch $@

check-svn-0.3.stamp: svn-0.3.stamp git-0.3.stamp
	diff -x .svn -x .git -r --brief svn/0.3/kom++ 0.3-kom++.git
	touch $@

check-cvs-0.5.stamp: cvs-0.5.stamp git-0.5.stamp
	diff -x CVS -x .git -r --brief cvs/0.5/kom++ 0.5-kom++.git
	touch $@

check-svn-0.5.stamp: svn-0.5.stamp git-0.5.stamp
	diff -x .svn -x .git -r --brief svn/0.5/kom++ 0.5-kom++.git
	touch $@

check-cvs-0.7.stamp: cvs-0.7.stamp git-0.7.stamp
	diff -x CVS -x .git -I '\$$Source:.*\$$' -r --brief \
	    -x parser \
	    -x aix3 \
	    -x aix4 \
	    -x dos_8x3 \
	    -x freebsd2 \
	    -x generic \
	    -x linux2 \
	    -x next3_3 \
	    cvs/0.7/kom++ 0.7-kom++.git
	touch $@

check-svn-0.7.stamp: svn-0.7.stamp git-0.7.stamp
	diff -x .svn -x .git -r --brief \
	    -x parser \
	    -x aix3 \
	    -x aix4 \
	    -x dos_8x3 \
	    -x freebsd2 \
	    -x generic \
	    -x linux2 \
	    -x next3_3 \
	    svn/0.7/kom++ 0.7-kom++.git
	touch $@

check-cvs-0.7.post.2.stamp: cvs-0.7.post.2.stamp git-0.7.post.2.stamp
	diff -x CVS -x .git -r --brief \
	    cvs/0.7.post.2/kom++ 0.7.post.2-kom++.git
	touch $@

check-svn-0.7.post.2.stamp: svn-0.7.post.2.stamp git-0.7.post.2.stamp
	diff -x .svn -x .git -r --brief -b \
	    svn/0.7.post.2/kom++ 0.7.post.2-kom++.git
	touch $@

check-cvs-0.7.post.4.stamp: cvs-0.7.post.4.stamp git-0.7.post.4.stamp
	diff -x CVS -x .git -r --brief \
	    -x lm.3 -x py.1 -x sc.10 -x sc.9 \
	    cvs/0.7.post.4/kom++ 0.7.post.4-kom++.git
	touch $@

check-svn-0.7.post.4.stamp: svn-0.7.post.4.stamp git-0.7.post.4.stamp
	diff -x .svn -x .git -r --brief \
	    -x lm.3 -x py.1 -x sc.10 -x sc.9 \
	    svn/0.7.post.4/kom++ 0.7.post.4-kom++.git
	touch $@

check-cvs-0.7.post.5.stamp: cvs-0.7.post.5.stamp git-0.7.post.5.stamp
	diff -x CVS -x .git -r --brief cvs/0.7.post.5/kom++ 0.7.post.5-kom++.git
	touch $@

check-svn-0.7.post.5.stamp: svn-0.7.post.5.stamp git-0.7.post.5.stamp
	diff -x .svn -x .git -r --brief svn/0.7.post.5/kom++ 0.7.post.5-kom++.git
	touch $@

check-cvs-0.7.post.6.stamp: cvs-0.7.post.6.stamp git-0.7.post.6.stamp
	diff -x CVS -x .git -r --brief cvs/0.7.post.6/kom++ 0.7.post.6-kom++.git
	touch $@

check-svn-0.7.post.6.stamp: svn-0.7.post.6.stamp git-0.7.post.6.stamp
	diff -x .svn -x .git -r --brief svn/0.7.post.6/kom++ 0.7.post.6-kom++.git
	touch $@

check-cvs-snapshot-960712.stamp: cvs-snapshot-960712.stamp git-snapshot-960712.stamp
	diff -x CVS -x .git -r --brief \
	    cvs/snapshot-960712/kom++ snapshot-960712-kom++.git
	touch $@

check-svn-snapshot-960712.stamp: svn-snapshot-960712.stamp git-snapshot-960712.stamp
	diff -x .svn -x .git -r --brief \
	    svn/snapshot-960712/kom++ snapshot-960712-kom++.git
	touch $@

check-cvs-ERRHIDE.stamp: cvs-ERRHIDE.stamp git-ERRHIDE.stamp
	diff -x CVS -x .git -r --brief cvs/ERRHIDE/kom++ ERRHIDE-kom++.git
	touch $@

check-svn-ERRHIDE.stamp: svn-ERRHIDE.stamp git-ERRHIDE.stamp
	diff -x .svn -x .git -r --brief svn/ERRHIDE/kom++ ERRHIDE-kom++.git
	touch $@

check-cvs-isc.stamp: cvs-isc.stamp git-isc.stamp
	diff -x CVS -x .git -r --brief cvs/isc/kom++ isc-kom++.git
	touch $@

check-svn-isc.stamp: svn-isc.stamp git-isc.stamp
	diff -x .svn -x .git -r --brief svn/isc/kom++ isc-kom++.git
	touch $@

check-cvs-python.stamp: cvs-python-1.4.stamp git-python.stamp
	diff -x CVS -x .git -r --brief cvs/python-1.4/kom++ python-kom++.git
	touch $@

check-svn-python.stamp: svn-python-1.4.stamp git-python.stamp
	diff -x .svn -x .git -r --brief -b \
	    svn/python-1.4/kom++ python-kom++.git
	touch $@

check-cvs-tcl.stamp: cvs-tcl.stamp git-tcl.stamp
	diff -x CVS -x .git -r --brief cvs/tcl/kom++ tcl-kom++.git
	touch $@

check-svn-tcl.stamp: svn-tcl.stamp git-tcl.stamp
	diff -x .svn -x .git -r --brief svn/tcl/kom++ tcl-kom++.git
	touch $@

check-cvs-isc-0.98.3.stamp: cvs-isc-0.98.3.stamp git-isc-0.98.3.stamp
	diff -x CVS -x .git -r --brief cvs/isc-0.98.3/kom++ isc-0.98.3-kom++.git
	touch $@

check-svn-isc-0.98.3.stamp: svn-isc-0.98.3.stamp git-isc-0.98.3.stamp
	diff -x .svn -x .git -r --brief svn/isc-0.98.3/kom++ isc-0.98.3-kom++.git
	touch $@

check-cvs-isc-1.00.stamp: cvs-isc-1.00.stamp git-isc-1.00.stamp
	diff -x CVS -x .git -r --brief cvs/isc-1.00/kom++ isc-1.00-kom++.git
	touch $@

check-svn-isc-1.00.stamp: svn-isc-1.00.stamp git-isc-1.00.stamp
	diff -x .svn -x .git -r --brief svn/isc-1.00/kom++ isc-1.00-kom++.git
	touch $@

check-cvs-isc-1.01.stamp: cvs-isc-1.01.stamp git-isc-1.01.stamp
	diff -x CVS -x .git -r --brief cvs/isc-1.01/kom++ isc-1.01-kom++.git
	touch $@

check-svn-isc-1.01.stamp: svn-isc-1.01.stamp git-isc-1.01.stamp
	diff -x .svn -x .git -r --brief svn/isc-1.01/kom++ isc-1.01-kom++.git
	touch $@

check-cvs-python-1.4.stamp: cvs-python-1.4.stamp git-python-1.4.stamp
	diff -x CVS -x .git -r --brief \
	    cvs/python-1.4/kom++ python-1.4-kom++.git
	touch $@

check-svn-python-1.4.stamp: svn-python-1.4.stamp git-python-1.4.stamp
	diff -x .svn -x .git -r --brief -b \
	    svn/python-1.4/kom++ python-1.4-kom++.git
	touch $@

check-cvs-python-1.4b1.stamp: cvs-python-1.4b1.stamp git-python-1.4b1.stamp
	diff -x CVS -x .git -r --brief -I '\$$Source:.*\$$' \
	    cvs/python-1.4b1/kom++ python-1.4b1-kom++.git
	touch $@

check-svn-python-1.4b1.stamp: svn-python-1.4b1.stamp git-python-1.4b1.stamp
	diff -x .svn -x .git -r --brief \
	    svn/python-1.4b1/kom++ python-1.4b1-kom++.git
	touch $@

check-cvs-python-1.4b3.stamp: cvs-python-1.4b3.stamp git-python-1.4b3.stamp
	diff -x CVS -x .git -r --brief -I '\$$Source:.*\$$' \
	    cvs/python-1.4b3/kom++ python-1.4b3-kom++.git
	touch $@

check-svn-python-1.4b3.stamp: svn-python-1.4b3.stamp git-python-1.4b3.stamp
	diff -x .svn -x .git -r --brief \
	    svn/python-1.4b3/kom++ python-1.4b3-kom++.git
	touch $@

check-cvs-tcl7.5a2.stamp: cvs-tcl7.5a2.stamp git-tcl7.5a2.stamp
	diff -x CVS -x .git -r --brief \
	    cvs/tcl7.5a2/kom++ tcl7.5a2-kom++.git
	touch $@

check-svn-tcl7.5a2.stamp: svn-tcl7.5a2.stamp git-tcl7.5a2.stamp
	diff -x .svn -x .git -r --brief \
	    svn/tcl7.5a2/kom++ tcl7.5a2-kom++.git
	touch $@

check-cvs-last-cvs.stamp: cvs-last-cvs.stamp git-last-cvs.stamp
	diff -x CVS -x .git -r --brief \
	    cvs/last-cvs/kom++ last-cvs-kom++.git
	touch $@

check-svn-last-cvs.stamp: svn-last-cvs.stamp git-last-cvs.stamp
	diff -x .svn -x .git -r --brief \
	    svn/last-cvs/kom++ last-cvs-kom++.git
	touch $@

check-svn-last-svn.stamp: svn-last-svn.stamp git-last-svn.stamp
	diff -x .svn -x .git -r --brief \
	    svn/last-svn/kom++ last-svn-kom++.git
	touch $@

check-dist-0.1.stamp: git-0.1.stamp kom++-0.1.stamp
	diff -x .git -x .cvsignore -x '*~' -x '.#*' -x RELEASING -x error-hiders \
	    -r --brief \
	    kom++-0.1 0.1-kom++.git
	touch $@

check-dist-0.2.stamp: git-0.2.stamp kom++-0.2.stamp
	diff -x .git -x .cvsignore -x RELEASING \
	    -r --brief \
	    kom++-0.2 0.2-kom++.git
	touch $@

check-dist-0.3.stamp: git-0.3.stamp kom++-0.3.stamp
	diff -x .git -x .cvsignore -x Makefile -x Mailinglist -x RELEASING \
	    -x other-clients -x libcompat \
	    -r --brief \
	    kom++-0.3 0.3-kom++.git
	touch $@

check-dist-0.4.stamp: git-0.4.stamp kom++-0.4.stamp
	diff -x .git -x .cvsignore -x strerror.c -x Mailinglist \
	    -x Makefile -x other-clients -x index.html -x tclIndex \
	    -r --brief \
	    kom++-0.4 0.4-kom++.git
	touch $@

check-dist-0.5.stamp: git-0.5.stamp kom++-0.5.stamp
	diff -x .git -x .cvsignore -x strerror.c -x Mailinglist \
	    -x Makefile -x other-clients -x tclIndex \
	    -r --brief \
	    kom++-0.5 0.5-kom++.git
	touch $@

check-dist-0.6.stamp: git-0.6.stamp kom++-0.6.stamp
	diff -x .git -x .cvsignore -x Makefile -x tclIndex \
	    -x other-clients -x strerror.c -x Mailinglist \
	    -I '\$$Source:.*\$$' \
	    -r --brief \
	    kom++-0.6 0.6-kom++.git
	touch $@

check-dist-0.7.stamp: git-0.7.stamp kom++-0.7.stamp
	diff -x .git -x .cvsignore -x Makefile -x tclIndex \
	    -x other-clients -x strerror.c -x Mailinglist \
	    -I '\$$Source:.*\$$' \
	    -r --brief \
	    kom++-0.7 0.7-kom++.git
	touch $@

check-dist-0.7.post.4.stamp: git-0.7.post.4.stamp kom++-0.7.post.4.stamp
	diff -x .git -x .cvsignore -x Mailinglist -x www -x py-clients \
	    -x pmg.py -x mkmi -x genclasses -x doc -x RELEASING \
	    -x PROJECTS -x PROBLEMS -x Makefile.py -x TEMPLATE.cc \
	    -x TEMPLATE.h -x get_session_info_ident_q.h \
	    -x set_priv_bits_q.h -x cmd-table.h -x error-hider.h \
	    -x error-hider.cc -x error-hider.icc -x session-info-ident.h \
	    -x session-info.h -x sparsemap.cc -x sparsemap.h \
	    -x sparsemap.icc -x butwin.tcl -x kom_support.tcl \
	    -x lblentry.tcl -x mkindex -x tcl-extensions \
	    -x tkom-appinit.c -x tkom.tcl -x tkom_commands_conf.tcl \
	    -x tkom_commands_file.tcl -x tkom_commands_misc.tcl \
	    -x tkom_commands_read.tcl -x tkom_commands_session.tcl \
	    -x tkom_commands_text.tcl -x tkom_commands_write.tcl \
	    -x tkom_main_window.tcl -x tkom_write_window.tcl \
	    -x transient_window.tcl -x www.tcl \
	    -r --brief \
	    kom++-0.7.post.4 0.7.post.4-kom++.git
	touch $@

check-dist-0.7.post.5.stamp: git-0.7.post.5.stamp kom++-0.7.post.5.stamp
	diff -x .git -x .cvsignore -x Mailinglist -x TEMPLATE.cc \
	    -x TEMPLATE.h -x get_session_info_ident_q.h \
	    -x set_priv_bits_q.h -x cmd-table.h -x error-hider.h \
	    -x error-hider.cc -x error-hider.icc -x session-info-ident.h \
	    -x session-info.h -x sparsemap.cc -x sparsemap.h \
	    -x sparsemap.icc -x PROJECTS -x genclasses -x curmgr.py \
	    -x cursestest.py -x t99699.py -x testsuite -x login.spec \
	    -x siteconfig.py \
	    -r --brief \
	    kom++-0.7.post.5 0.7.post.5-kom++.git
	touch $@

check-dist-0.7.post.6.stamp: git-0.7.post.6.stamp kom++-0.7.post.6.stamp
	diff -x .git -x .cvsignore -x Mailinglist -x TEMPLATE.cc \
	    -x TEMPLATE.h -x get_session_info_ident_q.h \
	    -x set_priv_bits_q.h -x cmd-table.h -x error-hider.h \
	    -x error-hider.cc -x error-hider.icc -x session-info-ident.h \
	    -x session-info.h -x sparsemap.cc -x sparsemap.h \
	    -x sparsemap.icc -x PROJECTS -x genclasses -x curmgr.py \
	    -x cursestest.py -x t99699.py -x testsuite -x login.spec \
	    -x kom++.html \
	    -r --brief \
	    kom++-0.7.post.6 0.7.post.6-kom++.git
	touch $@

check-commit-msg.stamp: 94.stamp 95.stamp
	(cd 94-kom++.git && git log --all --format='==%n%t%n%B') > pre.txt
	(cd 95-kom++.git && git log --all --format='==%n%t%n%B') > post.txt
	wdiff -3 pre.txt post.txt
	touch $@

check: check-cvs-0.1.stamp check-svn-0.1.stamp \
	check-cvs-0.3.stamp check-svn-0.3.stamp \
	check-cvs-0.5.stamp check-svn-0.5.stamp \
	check-cvs-0.7.stamp check-svn-0.7.stamp \
	check-cvs-0.7.post.2.stamp check-svn-0.7.post.2.stamp \
	check-cvs-0.7.post.4.stamp check-svn-0.7.post.4.stamp \
	check-cvs-0.7.post.5.stamp check-svn-0.7.post.5.stamp \
	check-cvs-0.7.post.6.stamp check-svn-0.7.post.6.stamp \
	check-cvs-isc.stamp check-svn-isc.stamp \
	check-cvs-python.stamp check-svn-python.stamp \
	check-cvs-tcl.stamp check-svn-tcl.stamp \
	check-cvs-isc-0.98.3.stamp check-svn-isc-0.98.3.stamp \
	check-cvs-isc-1.00.stamp check-svn-isc-1.00.stamp \
	check-cvs-isc-1.01.stamp check-svn-isc-1.01.stamp \
	check-cvs-python-1.4.stamp check-svn-python-1.4.stamp \
	check-cvs-python-1.4b1.stamp check-svn-python-1.4b1.stamp \
	check-cvs-python-1.4b3.stamp check-svn-python-1.4b3.stamp \
	check-cvs-tcl7.5a2.stamp check-svn-tcl7.5a2.stamp \
	check-cvs-snapshot-960712.stamp check-svn-snapshot-960712.stamp \
	check-cvs-last-cvs.stamp check-svn-last-cvs.stamp \
	check-cvs-ERRHIDE.stamp check-svn-ERRHIDE.stamp \
	check-svn-last-svn.stamp \
	check-dist-0.1.stamp \
	check-dist-0.2.stamp \
	check-dist-0.3.stamp \
	check-dist-0.4.stamp \
	check-dist-0.5.stamp \
	check-dist-0.6.stamp \
	check-dist-0.7.stamp \
	check-dist-0.7.post.4.stamp \
	check-dist-0.7.post.5.stamp \
	check-dist-0.7.post.6.stamp \
	check-commit-msg.stamp
